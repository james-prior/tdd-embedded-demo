/*
Refleshing out ideas I have had for test-driving embedded C.
This time I have actually been looking at the Unity test framework for C.

2020-02-25

Imagine where every read and write of a variable may only be done wih
functions. For example, if variable foo could only be read by calling
read_foo() and writing to foo could only be done by calling write_foo(value).

The good thing about those functions is that it makes it easy for testing to
track all reads and writes.

And those read and write functions are ugly to me.

Now instead of an ordinary variable, consider accessing a hardware register.
Again, accessing only with functions makes it easier for testing to track all
reads and writes. And those read and write functions are just as ugly to me as
for accessing ordinary variables.

So ideally, I have been wanting to track accesses to hardware registers for
testing, and retain the ordinary syntax of reading and writing registers. I
just now realize that I should want that for volatile variables such as for
those shared between main line code and an interrupt service routine.

I have not achieved that goal. The closest I have come are having separate
names for reading and writing hardware registers (and volatile variables). For
example, the foo_w and foo_r macros below. I recently fleshed out
test_assert_equal_iolog for the first time. I have had these ideas for months.

A new epiphany is that the fancy macros for tracking every access including the
order is not necessary for the vast majority of tested. For the vast majority
of things tested, just checking the values after executing the code being
tested suffices. For the vast majority of cases, one does need to check
intermediate values or the order in which accesses are done. For this much more
common case, having a pointer for the variable that could point to a hardware
register in production and to an ordinary variable during testing, as Grenning
does with virtualLeds in Chapter 2 of Test-Driven Development for Embedded C,
would suffice.

Which makes me wonder if I should set my complicated macros below aside to deal
with the common cases first, coming back to the complicated macros when I
really need them. Without the complicated macros, I can use the ordinary syntax
for reading and writing variables.
*/

#define FALSE (0)
#define TRUE (!FALSE)

#define ARRAY_LENGTH(x) (sizeof(x) / sizeof(*(x)))

struct io_log_struct {
    char *port_name;
    unsigned *port_address;
    unsigned read_not_write;
    unsigned value;
}

struct io_log_struct io_log[20];
unsigned io_log_i;

#define generic_w(port_name) *( \
    io_log_i = ( \
        io_log_i < ARRAY_LENGTH(io_log) \
        ? io_log_i + 1 : ARRAY_LENGTH(io_log) \
    ), \
    io_log[io_log_i - 1].port_name = #port_name, \
    io_log[io_log_i - 1].port_address = &(port_name), \
    io_log[io_log_i - 1].read_not_write = FALSE, \
    &io_log[io_log_i - 1].value \
)

#define foo_w (generic_w(foo))
#define bar_w (generic_w(bar))

unsigned foo_read_value[20];
unsigned foo_read_value_i;

/*^^^ Does the comma operator guarantee that for a, b, that writes within a are
*^^^  done before b is evaluated?
#define generic_r(port_name) ( \
    port_name ## _read_value_i = ( \
        port_name ## _read_value_i < ARRAY_LENGTH(port_name ## _read_value) \
        ? port_name ## _read_value_i + 1 \
        : ARRAY_LENGTH(port_name ## _read_value) \
    ), \
    io_log_i = ( \
        io_log_i < ARRAY_LENGTH(io_log) \
        ? io_log_i + 1 : ARRAY_LENGTH(io_log) \
    ), \
    io_log[io_log_i - 1].port_name = #port_name, \
    io_log[io_log_i - 1].port_address = &(port_name), \
    io_log[io_log_i - 1].read_not_write = FALSE, \
    io_log[io_log_i - 1].value = foo_read_value[foo_read_value_i - 1] \
)

#define foo_r (generic_r(foo))
#define bar_r (generic_r(bar))

/* don't know if I can nest #defines */
#define setup_test_port(port_name) \
    #define port_name ## _w (generic_w(port_name)) \
    unsigned port_name ## _read_value[20]; \
    unsigned port_name ## _read_value_i; \
    #define port_name ## _r (generic_r(port_name))

void test_assert_equal_iolog(
    unsigned expected_io_log_n,
    struct io_log_struct *expected_io_log)
{
    char message[200];

    for (unsigned i = 0; i < expected_io_log_n && i < io_log_i; i++) {
        struct io_log_struct *s = io_log[i];
        struct io_log_struct *t = expected_io_log[i];

        if (strcmp(s->port_name, t->port_name) == 0
        && s->port_address == t->port_address
        && !!s->read_not_write == !!t->read_not_write
        && s->value == t->value) {
            TEST_ASSERT_TRUE()
            continue;
        }

        sprintf(
            message,
            "io_log[%u]: "
            "expected: (%s 0x%02X %s '%s'@0x%02X) "
            "actual: (%s 0x%02X %s '%s'@0x%02X)",
            i,
            t->read_not_write ? "read" : "write",
            t->value,
            t->read_not_write ? "from" : "to",
            t->port_name,
            t->port_address,
            s->read_not_write ? "read" : "write",
            s->value,
            s->read_not_write ? "from" : "to",
            s->port_name,
            s->port_address
        );
        TEST_FAIL_MESSAGE(message)
    }

    TEST_ASSERT_EQUAL_UINT(expected_io_log_n, io_log_i)

    /*??? check that stuff past expected_io_log_n is unchanged? */
}

#if 0

#if testing
volatile uint8_t foo; // regular variable
#define foo_w (generic_w(foo))
#define foo_r (generic_r(foo))
#else
#define foo (*(volatile uint8_t *)(0x1234))
#define foo_w (foo)
#define foo_r (foo)
#endif

#define LED_MASK (1<<5)
#define LED_ON   (1<<5)
#define LED_OFF  (1<<5)
#define led_port_w (portb_w)
#define led_port_r (portb_r)

Will compiler be smart enough to optimize following to a single machine
language instructions?

turn on led:
led_port_w = (led_port_r & ~LED_MASK) | LED_ON;

turn off led:
led_port_w = (led_port_r & ~LED_MASK) | LED_OFF;

#endif
