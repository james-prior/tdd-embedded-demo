The repo for the
[Arduino CLI stuff](https://gitlab.com/james-prior/arduino-cli)
has moved to
[https://gitlab.com/james-prior/arduino-cli](https://gitlab.com/james-prior/arduino-cli).

---

TDD for Embedded C Demo
=======================

`iolog.c` is interesting. Need to try it out with
[unity](https://www.throwtheswitch.org/unity).
